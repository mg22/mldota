# Predikcia výsledku zápasu Doty 2 na základe hrdinov #
## Projekt na predmet Strojové učenie ##

Projekt má za úlohu použiť metódy strojového učenia na predikciu výsledku zápasu Doty 2 z informácie, ktorých hrdinov si hráči vybrali.


**Poznámka:** keďže mal projekt formu článku, repozitár je skôr súbor rôznych pokusov než jedna fungujúca aplikácia.
 
### Zbieranie dát ###
Relevantný kód je celý v skripte [bot.py](https://bitbucket.org/mg22/mldota/src/cef9788bb79fd1df32f6100df7c44b4e28514d0f/data_collect/bot.py?at=master&fileviewer=file-view-default). Je určený do cron-u, keď po spustení stiahne zoznam 500 posledných zápasov, cez všetky prejde a pre relevantné zápasy uloži podrobnejšie informácie do Mongo databázy.

Pre korektné fungovanie je treba mať nainštalovanú Mongo databázu, pymongo a dota2api wrapper pre komunikáciu so Steam API. Ďalej je treba mať Steam API key a pred spustením vhodne upraviť cestu k log file-u.

### ML kód ###
Hlavná časť repozitára, kde som testoval jednotlivé modely. Je v tom trochu bordel, preto tu popíšem, na čo jednotlivé súbory slúžia. Súbory, ktoré tu nie sú spomenuté boli pokusy, ktoré k ničomu neviedli (a nie sú spomenuté ani v reporte) ale pre future-reference som ich nevymazal.

Prerekvizity sú sklearn pre všetko a Mongo databáza a pymongo pre [prepare_data.py](https://bitbucket.org/mg22/mldota/src/cef9788bb79fd1df32f6100df7c44b4e28514d0f/machine_learning/prepare_data.py?at=master&fileviewer=file-view-default).

V súbore data sú exportované dáta z Mongo databázy v numpy array formáte, o to sa stará skript [prepare_data.py](https://bitbucket.org/mg22/mldota/src/cef9788bb79fd1df32f6100df7c44b4e28514d0f/machine_learning/prepare_data.py?at=master&fileviewer=file-view-default). Defaultne skoro všetky skripty používajú ako dataset all_train.npz, all_test.npz. Skripty testujúce rôzne modely by mali byť pomerne čitateľné, aby sa dali ľahko upraviť na predikovanie vlastných dát.

V [util.py](https://bitbucket.org/mg22/mldota/src/cef9788bb79fd1df32f6100df7c44b4e28514d0f/machine_learning/util.py?at=master&fileviewer=file-view-default) a [plot.py](https://bitbucket.org/mg22/mldota/src/cef9788bb79fd1df32f6100df7c44b4e28514d0f/machine_learning/plot.py?at=master&fileviewer=file-view-default) sú rôzne pomocné funkcie k testovaniu a grafom.

Súbory [generate_ngrams.py](https://bitbucket.org/mg22/mldota/src/cef9788bb79fd1df32f6100df7c44b4e28514d0f/machine_learning/generate_ngrams.py?at=master&fileviewer=file-view-default) a [generate_synergy.py](https://bitbucket.org/mg22/mldota/src/cef9788bb79fd1df32f6100df7c44b4e28514d0f/machine_learning/generate_synergy.py?at=master&fileviewer=file-view-default) slúžia na spočítanie štatistiky o k-ticiach hrdinov v dátach a podľa štatistiky vyrobenie z datasetu [pick hrdinov na oboch stranách, ...] -> [pick hrdinov na jednej strane, ..., ...], [synergia prislúchajúceho picku, ..., ...].

Súbory nazvané po jednotlivých ML metódach testujú danú metódu, väčšinou cross-validácia a na testovacej množine.

Súbor [synergy.py](https://bitbucket.org/mg22/mldota/src/cef9788bb79fd1df32f6100df7c44b4e28514d0f/machine_learning/synergy.py?at=master&fileviewer=file-view-default) testuje rôzne váhy synergie viacerých metód, [mixed.py](https://bitbucket.org/mg22/mldota/src/cef9788bb79fd1df32f6100df7c44b4e28514d0f/machine_learning/mixed.py?at=master&fileviewer=file-view-default) mixovanie viacerých modelov.


### Materiály, text ###
V príslušných adresároch sú obe predchádzajúce práce, pdf-ko a tex reportu.