mods_json = {
    "mods": [
        {
            "id" : 0,
            "name" : "Unknown"
        },
        {
            "id" : 1,
            "name" : "All Pick"
        },
        {
            "id" : 2,
            "name" : "Captains Mode"
        },
        {
            "id" : 3,
            "name" : "Random Draft"
        },
        {
            "id" : 4,
            "name" : "Single Draft"
        },
        {
            "id" : 5,
            "name" : "All Random"
        },
        {
            "id" : 6,
            "name" : "?? INTRO/DEATH ??"
        },
        {
            "id" : 7,
            "name" : "The Diretide"
        },
        {
            "id" : 8,
            "name" : "Reverse Captains Mode"
        },
        {
            "id" : 9,
            "name" : "Greeviling"
        },
        {
            "id" : 10,
            "name": "Tutorial"
        },
        {
            "id" : 11,
            "name" : "Mid Only"
        },
        {
            "id" : 12,
            "name" : "Least Played"
        },
        {
            "id" : 13,
            "name" : "New Player Pool"
        },
        {
            "id" : 14,
            "name" : "Compendium Matchmaking"
        },
        {
            "id": 15,
            "name": "Custom"
        },
        {
            "id": 16,
            "name": "Captains Draft"
        },
        {
            "id": 17,
            "name": "Balanced Draft"
        },
        {
            "id": 18,
            "name": "Ability Draft"
        },
        {
            "id": 19,
            "name": "?? Event ??"
        },
        {
            "id": 20,
            "name": "All Random Death Match"
        },
        {
            "id": 21,
            "name": "1vs1 Solo Mid"
        },
        {
            "id": 22,
            "name": "Ranked All Pick"
        }
    ]
}
id_to_mode_name = {}
for x in mods_json['mods']:
    id_to_mode_name[x['id']] = x['name']
print id_to_mode_name

mode_id_to_name = {
    0: 'Unknown', 
    1: 'All Pick', 
    2: 'Captains Mode', 
    3: 'Random Draft', 
    4: 'Single Draft', 
    5: 'All Random', 
    6: '?? INTRO/DEATH ??', 
    7: 'The Diretide', 
    8: 'Reverse Captains Mode', 
    9: 'Greeviling', 
    10: 'Tutorial', 
    11: 'Mid Only', 
    12: 'Least Played', 
    13: 'New Player Pool', 
    14: 'Compendium Matchmaking', 
    15: 'Custom', 
    16: 'Captains Draft', 
    17: 'Balanced Draft', 
    18: 'Ability Draft', 
    19: '?? Event ??', 
    20: 'All Random Death Match', 
    21: '1vs1 Solo Mid', 
    22: 'Ranked All Pick'
}
# print json