import logging
import os
import sys
import time

import dota2api
from pymongo import MongoClient


logger = None
api = None
matches_collection = None
timeout_matches = None

# 'good' modes according to previous papers, added 22
suitable_modes = set([1, 2, 3, 4, 5, 12, 16, 22])
# game mode id to humanly readable name 
# taken from: https://github.com/kronusme/dota2-api/blob/master/data/mods.json
# as pointed in: http://dev.dota2.com/showthread.php?t=58317
mode_name = {
    0: 'Unknown', 
    1: 'All Pick', 
    2: 'Captains Mode', 
    3: 'Random Draft', 
    4: 'Single Draft', 
    5: 'All Random', 
    6: '?? INTRO/DEATH ??', 
    7: 'The Diretide', 
    8: 'Reverse Captains Mode', 
    9: 'Greeviling', 
    10: 'Tutorial', 
    11: 'Mid Only', 
    12: 'Least Played', 
    13: 'New Player Pool', 
    14: 'Compendium Matchmaking', 
    15: 'Custom', 
    16: 'Captains Draft', 
    17: 'Balanced Draft', 
    18: 'Ability Draft', 
    19: '?? Event ??', 
    20: 'All Random Death Match', 
    21: '1vs1 Solo Mid', 
    22: 'Ranked All Pick'
}

trial_threshold = 0


def setup():
    global logger, api, matches_collection, timeout_matches

    # logging
    log_filename = '/home/mg/Documents/skola/strojaky/mldota/data_collect/logs/bot.log'
    handler = logging.FileHandler(log_filename)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger = logging.getLogger('bot')
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)

    # dota2api setup
    steam_api_key = os.getenv('DOTA2_API_KEY')
    if not steam_api_key:
        print 'DOTA2_API_KEY system variable is not properly set'
        sys.exit(1)      
    api = dota2api.Initialise(steam_api_key) 

    # MongoDB setup
    client = MongoClient()
    db = client.ml_dota
    matches_collection = db.matches
    timeout_matches = db.timeout_matches


def suitable_match(match_details):
    # only suitable modes
    game_mode = match_details['game_mode'] 
    if game_mode not in suitable_modes:
        # logger.debug('Not suitable game mode {}'.format(match_details['game_mode']))
        return False, 'bad game mode, {}'.format(mode_name[game_mode])
    # no leavers/afk/disconected
    for player in match_details['players']:
        if player['leaver_status'] != 0:
            # logger.debug('Leaver')
            return False, 'leaver'
    # at least 15 minutes
    if match_details['duration'] < 900:
        # logger.debug('Too short game')
        return False, 'short game'
    return True, None


def process_match(match_id):
    trials = 0
    while True:
        try:
            trials += 1
            match_details = api.get_match_details(match_id)
            break
        except dota2api.src.exceptions.APITimeoutError:
            if trials > trial_threshold:
                logger.debug('Too much HTTP 503 timeouts, skipping match: {}'.format(match_id))
                timeout_matches.insert({'match_id': match_id})
                # sys.exit(1)
                return
            logger.debug('Match details HTTP 503 timeout, waiting and then trying again')
            time.sleep(30.0)

    suitable, reason = suitable_match(match_details)
    if not suitable:
        logger.debug('Rejected match: {0}, reason: {1}'.format(match_id, reason))
        return
    matches_collection.insert(match_details)

    game_mode = match_details['game_mode']
    logger.debug('Added match: {0}, game mode: {1}'.format(match_id, mode_name[game_mode]))


def main():
    starting_match_id = None
    # all pick
    game_mode = 1

    logger.debug('##### Starting')
    while True:
        msg = '## Getting match history for match id: {}'.format(
            starting_match_id
        )
        logger.debug(msg)

        time.sleep(1.0)
        trials = 0
        while True:
            try:
                trials += 1
                result = api.get_match_history(
                    start_at_match_id=starting_match_id,
                    # game_mode=game_mode,
                    skill=3,
                    min_players=10
                )
                break
            except dota2api.src.exceptions.APITimeoutError:
                if trials > trial_threshold:
                    logger.debug('Too much HTTP 503 timeouts, exiting')
                    sys.exit(1)
                logger.debug('Match history HTTP 503 timeout, waiting and then trying again')
                time.sleep(30.0)

        status = result['status']
        if status != 1:
            logger.debug('Match history query error on id {0}, error {1}'.format(starting_match_id, status))
        matches = result['matches']
        if len(matches) == 0:
            logger.debug('#### Finished processing all recent matches')
            sys.exit(0)

        for match in matches:
            match_id = match['match_id']
            # logger.debug('Processing match: {}'.format(match_id))

            if matches_collection.find_one({'match_id': match_id}) is not None:
                # we have this match already in collection, hence all matches
                # after him
                logger.debug('Match {} already in collection, exiting'.format(match_id))
                sys.exit(0)
                # continue

            time.sleep(2.0)
            process_match(match_id)

        logger.debug('## Processed all matches from current \'page\', last match id: {}'.format(match_id))
        starting_match_id = match_id - 1

    logger.debug('##### Finished processing all recent matches')


if __name__ == '__main__':
    setup()
    main()
