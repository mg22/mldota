import logging
import dota2api
from time import sleep
from pymongo import MongoClient

from bot import suitable_match, mode_name


def process_match(match_id):
    if match_collection.find_one({'match_id': match_id}) is not None:
        return True

    trials = 0
    while True:
        try:
            trials += 1
            match_details = api.get_match_details(match_id)
            break
        except dota2api.src.exceptions.APITimeoutError:
            logger.debug('Match details HTTP 503 timeout, waiting and then trying again')
            sleep(30.0)
            if trials > trial_threshold:
                logger.debug('Too much HTTP 503 timeouts, skipping match: {}'.format(match_id))
                timeout_matches.insert({'match_id': match_id})
                return False

    suitable, reason = suitable_match(match_details)
    if not suitable:
        logger.debug('Rejected match: {0}, reason: {1}'.format(match_id, reason))
        return True
    match_collection.insert(match_details)

    game_mode = match_details['game_mode']
    logger.debug('Added match: {0}, game mode: {1}'.format(match_id, mode_name[game_mode]))
    return True

trial_threshold = 20

handler = logging.StreamHandler()
logger = logging.getLogger('bot')
logger.setLevel(logging.DEBUG)
logger.addHandler(handler)

# api setup
api = dota2api.Initialise('2188A5E4DB01C9E458873844DC720E2C') 
# MongoDB setup
client = MongoClient()
db = client.ml_dota
match_collection = db.matches
timeout_matches = db.timeout_matches

for match in timeout_matches.find():
    processed = process_match(match['match_id'])
    if processed:
        timeout_matches.remove(match)
    sleep(2.0)