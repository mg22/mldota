import sys
import pickle
import numpy as np
from time import time
from itertools import product
from sklearn import cross_validation
from sklearn.metrics import accuracy_score
from sklearn.svm import LinearSVC, SVC
from sklearn.ensemble import RandomForestClassifier

import matplotlib.pyplot as plt
# from hpsklearn import hyperopt_estimator
from hyperopt import hp
from hyperopt.pyll import scope, as_apply

from sklearn.preprocessing import Normalizer
from sklearn.naive_bayes import GaussianNB 
from util import load_data
import util


train_X, train_y, test_X, test_y = load_data()
size = len(train_X)

nb = GaussianNB()
nb.fit(test_X, test_y)
util.print_metrics(test_y, nb.predict(test_X))
# util.cross_val_metrics(nb, train_X, train_y, cv=10)

