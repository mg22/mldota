from collections import defaultdict
import pickle
import sys
import os
import numpy as np
from time import time


from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import accuracy_score

import util
from util import HEROES


def compute_synergy(heroes, ngram_map, weight):
    winrates = [[] for _ in range(5)]
    for s in util.powerset(sorted(heroes)):
        if len(s) == 0:
            continue
        synergy = 0
        # if s in ngram_map and ngram_map[s]['total'] != 0:
            # synergy = float(ngram_map[s]['won']) / ngram_map[s]['total']
        winrates[len(s)-1].append(ngram_map[s])
    synergies = []
    num = 0
    denom = 0
    for i, l in enumerate(winrates):
        a, b = 0, 0
        for x in l:
            a += x['won']
            b += x['total']
        num += a * weight(i)
        denom += b * weight(i)

    return float(num) / denom


def generate_synergy_score(ngram_map, X, weight=None):
    if not weight:
        weight = lambda x: 2**x
    synergy = []
    X_picks = []
    for x in X:
        X_picks.append(x[:HEROES])
        radiant = util.extract_heroes(x[:HEROES])
        synergy.append(compute_synergy(radiant, ngram_map, weight))

        X_picks.append(x[HEROES:])
        dire = util.extract_heroes(x[HEROES:])
        synergy.append(compute_synergy(dire, ngram_map, weight))        

    indices = list(range(len(X_picks)))
    np.random.shuffle(indices)
    return np.array(X_picks)[indices], np.array(synergy)[indices]


def main():
    X_train, y_train, X_test, y_test = util.load_data()
    with open('data/all_train_ngram_map.pickle', 'rb') as f:
        ngram_map = pickle.load(f)
    X, s = generate_synergy_score(ngram_map, X_train)
    np.savez_compressed(
        'data/all_train_synergy.npz',
        X=X,
        synergy=s
    )


if __name__ == '__main__':
    main()