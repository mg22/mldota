# coding=utf-8

import numpy as np
from sklearn.learning_curve import learning_curve
import matplotlib.pyplot as plt


# from:
# http://scikit-learn.org/stable/auto_examples/model_selection/plot_learning_curve.html
def plot_learning_curve(estimator, title, X, y, ylim=None, cv=None,
                        n_jobs=1, train_sizes=np.linspace(.1, 1.0, 5)):
    """
    Generate a simple plot of the test and traning learning curve.

    Parameters
    ----------
    estimator : object type that implements the "fit" and "predict" methods
        An object of that type which is cloned for each validation.

    title : string
        Title for the chart.

    X : array-like, shape (n_samples, n_features)
        Training vector, where n_samples is the number of samples and
        n_features is the number of features.

    y : array-like, shape (n_samples) or (n_samples, n_features), optional
        Target relative to X for classification or regression;
        None for unsupervised learning.

    ylim : tuple, shape (ymin, ymax), optional
        Defines minimum and maximum yvalues plotted.

    cv : integer, cross-validation generator, optional
        If an integer is passed, it is the number of folds (defaults to 3).
        Specific cross-validation objects can be passed, see
        sklearn.cross_validation module for the list of possible objects

    n_jobs : integer, optional
        Number of jobs to run in parallel (default 1).
    """
    plt.figure()
    plt.title(title)
    if ylim is not None:
        plt.ylim(*ylim)
    plt.xlabel(u"Veľkosť trénovacej množiny")
    plt.ylabel(u"Skóre")
    train_sizes, train_scores, test_scores = learning_curve(
        estimator, X, y, cv=cv, n_jobs=n_jobs, train_sizes=train_sizes)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    plt.grid()

    plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std, alpha=0.1,
                     color="r")
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.1, color="g")
    plt.plot(train_sizes, train_scores_mean, 'o-', color="r",
             label=u"Trénovacie skóre")
    plt.plot(train_sizes, test_scores_mean, 'o-', color="g",
             label=u"Skóre z cross-validácie")

    plt.legend(loc="best")
    return plt




def plot_curves(curves, xlabel, ylabel, title, filename=None):
    plt.figure()
    for x, y, f, l in curves:
        plt.plot(x, y, f, label=l)
    plt.xlabel(xlabel, fontsize='large')
    plt.ylabel(ylabel, fontsize='large')
    plt.legend(loc='lower right')
    plt.title(title)

    plt.tight_layout()
    if filename:
        plt.savefig(filename)
    else:
        plt.show()


def show_or_save(curves, xlabel, ylabel, title):
    fn = None
    print sys.argv
    if len(sys.argv) > 1:
        fn = sys.argv[1]
    plot_curves(curves, xlabel, ylabel, title, fn)


def plot_regression():
    curves = []
    accs = np.load('logistic_regression_results_100.npz')
    sizes = accs['sizes']
    train_acc, test_acc = accs['train_acc'], accs['test_acc']
    curves.append([sizes, train_acc, 's-', u'Trénovacia množina'])
    curves.append([sizes, test_acc, 'p-', u'Testovacia množina'])
    
    plot_curves(curves, u'Veľkosť trénovacej množiny', u'Presnosť', u'Učiaca krivka logistickej regresie', 'logreg.pdf')
    # plot_curves(curves, u'Veľkosť trénovacej množiny', u'Presnosť', u'Učiaca krivka logistickej regresie', 'logreg.pdf')

    curves = []
    accs = np.load('logistic_regression_swap_results_100.npz')
    sizes = accs['sizes']
    train_acc, test_acc = accs['train_acc'], accs['test_acc']
    curves.append([sizes, train_acc, 's-', u'Trénovacia množina'])
    curves.append([sizes, test_acc, 'p-', u'Presnosť na testovacej množine'])

    accs = np.load('logistic_regression_swap_biased_results_100.npz')
    sizes = accs['sizes']
    train_acc, test_acc = accs['train_acc'], accs['test_acc']
    curves.append([sizes, train_acc, 's-', u'Presnosť na trénovacej množine'])
    curves.append([sizes, test_acc, 'p-', u'Presnosť na testovacej množine'])

    # plot_curves(curves, u'Veľkosť trénovacej množiny', u'Presnosť', u'Učiaca krivka logistickej regresie', 'logreg_swap.pdf')


def plot_linear_svm():
    curves = []
    accs = np.load('linear_svm_results_100.npz')
    sizes = accs['sizes']
    train_acc, test_acc = accs['train_acc'], accs['test_acc']
    curves.append([sizes, train_acc, 's-', u'Trénovacia množina'])
    curves.append([sizes, test_acc, 'p-', u'Testovacia množina'])
    
    plot_curves(curves, u'Veľkosť trénovacej množiny', u'Presnosť', u'Učiaca krivka lineárneho SVM')


if __name__ == '__main__':
    # plot_regression()
    plot_linear_svm()