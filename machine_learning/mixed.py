# coding=utf-8

import sys
import pickle
import numpy as np
from time import time
from itertools import product
from sklearn import cross_validation
from sklearn.metrics import accuracy_score
from sklearn.svm import LinearSVC, SVC
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import AdaBoostClassifier, RandomForestRegressor
from sklearn.naive_bayes import GaussianNB
from sklearn import metrics
from sklearn.linear_model import Ridge
from sklearn.ensemble import RandomForestClassifier
from sklearn.learning_curve import learning_curve
import matplotlib.pyplot as plt

# from hpsklearn import hyperopt_estimator
# import hpsklearn
# import hyperopt
from sklearn.preprocessing import Normalizer

import util
from util import HEROES
from generate_synergy import compute_synergy
from generate_ngrams import generate_ngram_winrate
from generate_synergy import generate_synergy_score


def swap_predict(estimator, X, non_swapped_weight=1.0):
    # probability of radiant team winning
    query_probs = estimator.predict_proba(X)[:,1]
    # swap radiant/dire heroes
    # cyclic shift of rows of X
    swapped_X = np.roll(X, len(X[0])/2, axis=1)
    # probability of radiant team winning if it was in dire position
    swapped_query_probs = estimator.predict_proba(swapped_X)[:,0]
    # weighted result probability
    probs = [((non_swapped_weight * q + (2-non_swapped_weight) * s)/2) for q, s in zip(query_probs, swapped_query_probs)]
    predicted = [(1 if p > 0.5 else 0) for p in probs]
    return predicted


def syn_predict(est, synergy, X):
    radiant_synergy = synergy.predict(X[:, :HEROES])
    dire_synergy = synergy.predict(X[:, HEROES:])
    syn_predict = [(0.5 + (r-d)/2) for r, d in zip(radiant_synergy, dire_synergy)]
    est_predict = est.predict_proba(X)[:,1]
    predicted = [1 if (p+q)/2 > 0.5 else 0 for p,q in zip(syn_predict, est_predict)]
    return predicted


# from:
# http://scikit-learn.org/stable/auto_examples/model_selection/plot_learning_curve.html
def plot_learning_curve_mod(estimator, synergy, title, X, y, ylim=None, cv=None,
                        n_jobs=1, train_sizes=np.linspace(.1, 1.0, 5)):
    """
    Generate a simple plot of the test and traning learning curve.

    Parameters
    ----------
    estimator : object type that implements the "fit" and "predict" methods
        An object of that type which is cloned for each validation.

    title : string
        Title for the chart.

    X : array-like, shape (n_samples, n_features)
        Training vector, where n_samples is the number of samples and
        n_features is the number of features.

    y : array-like, shape (n_samples) or (n_samples, n_features), optional
        Target relative to X for classification or regression;
        None for unsupervised learning.

    ylim : tuple, shape (ymin, ymax), optional
        Defines minimum and maximum yvalues plotted.

    cv : integer, cross-validation generator, optional
        If an integer is passed, it is the number of folds (defaults to 3).
        Specific cross-validation objects can be passed, see
        sklearn.cross_validation module for the list of possible objects

    n_jobs : integer, optional
        Number of jobs to run in parallel (default 1).
    """
    plt.figure()
    plt.title(title)
    if ylim is not None:
        plt.ylim(*ylim)
    plt.xlabel(u"Veľkosť trénovacej množiny")
    plt.ylabel(u"Skóre")

    def scorer(est, X, y):
        predicted = syn_predict(est, synergy, X)
        return accuracy_score(y, predicted)

    train_sizes, train_scores, test_scores = learning_curve(
        estimator, X, y, cv=cv, n_jobs=n_jobs, train_sizes=train_sizes, scoring=scorer)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    plt.grid()

    plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std, alpha=0.1,
                     color="r")
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.1, color="g")
    plt.plot(train_sizes, train_scores_mean, 'o-', color="r",
             label=u"Trénovacie skóre")
    plt.plot(train_sizes, test_scores_mean, 'o-', color="g",
             label=u"Skóre z cross-validácie")

    plt.legend(loc="best")
    return plt


def mix_syn_lr():
    X_train, y_train, X_test, y_test = util.load_data()    
    ngram_map = generate_ngram_winrate(X_train, y_train)
    X, s = generate_synergy_score(ngram_map, X_train)
    ridge = Ridge()
    ridge.fit(X, s)

    lr = LogisticRegression(dual=False)
    plot_learning_curve_mod(
        lr, ridge, u"Učiaca krivka RF so synergy", 
        X_train, y_train, cv=5, 
        train_sizes=np.linspace(40000, 80000, 50).astype(int)
    )
    plt.show()


def mix_syn_lr_test():
    X_train, y_train, X_test, y_test = util.load_data()    
    ngram_map = generate_ngram_winrate(X_train, y_train)
    X, s = generate_synergy_score(ngram_map, X_train)
    ridge = Ridge()
    ridge.fit(X, s)

    lr = LogisticRegression(dual=False)
    lr.fit(X_train, y_train)

    y_predicted = syn_predict(lr, ridge, X_test)
    util.print_metrics(y_test, y_predicted)


# mix_syn_lr()
mix_syn_lr_test()
    