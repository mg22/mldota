# coding=utf-8

import sys
import pickle
import numpy as np
from time import time

from sklearn import cross_validation
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
import matplotlib.pyplot as plt
from sklearn.learning_curve import learning_curve

import plot
import util


# Predict result of X with estimator
# as in Conley,Perry paper:
#   get probability of winning of radiant
#   then probability of winning radiant team if it were on dire position
#   radiant wins if average > 0.5 
def swap_predict(estimator, X, non_swapped_weight=1.0):
    # probability of radiant team winning
    query_probs = estimator.predict_proba(X)[:,1]
    # swap radiant/dire heroes
    # cyclic shift of rows of X
    swapped_X = np.roll(X, len(X[0])/2, axis=1)
    # probability of radiant team winning if it was in dire position
    swapped_query_probs = estimator.predict_proba(swapped_X)[:,0]
    # weighted result probability
    probs = [((non_swapped_weight * q + (2-non_swapped_weight) * s)/2) for q, s in zip(query_probs, swapped_query_probs)]
    predicted = [(1 if p > 0.5 else 0) for p in probs]
    return predicted


# Accuracy of estimator (fraction of correct out of all)
def swap_scorer(estimator, X, y, non_swapped_weight=1.0):
    predicted = swap_predict(estimator, X, non_swapped_weight)
    return accuracy_score(predicted, y)


train_X, train_y, test_X, test_y = util.load_data()

# test_accuracy(lr, train_X, train_y, test_X, test_y, points)

lr = LogisticRegression(dual=False)
plot.plot_learning_curve(
    lr,
    u'Učiaca krivka logistickej regresie',
    train_X,
    train_y,
    n_jobs=3,
    train_sizes=np.linspace(1000, 35000, 100).astype(int)
)

plt.show()


# with/without swap
# pred = lr.predict(test_X)
# pred = swap_predict(lr, test_X)
# util.print_metrics(test_y, pred)

# util.cross_val_metrics(lr, train_X, train_y)

