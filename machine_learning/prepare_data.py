import numpy as np

from pymongo import MongoClient


matches_collection = None
# hero id's are 1..113, exceptions are 
#   24 - nothing
#   108 - pit lord (not yet in game)
HEROES = 113
FEATURES = 2 * HEROES


def setup():
    global matches_collection
    
    client = MongoClient()
    db = client.ml_dota
    matches_collection = db.matches


def check_data(query=None):
    if not query:
        query = {}
    query_size = matches_collection.find(query).count()

    X = np.zeros((query_size, FEATURES)) 
    y = np.zeros(query_size)

    step = max(1, query_size / 20)
    print 'Loading data for query paramters {}, {} items'.format(query, query_size)
    for i, match in enumerate(matches_collection.find(query)):
        if i % step == 0:
            print '{:.0%}, {} matches loaded'.format(float(i)/query_size, i)
        if match['radiant_win']:
            y[i] = 1
        x = np.zeros(FEATURES, dtype=np.int8)
        n_side = [0, 0]
        for player in match['players']:
            hero_index = player['hero_id'] - 1
            side = 0
            if player['player_slot'] > 127:
                # player is dire
                hero_index += HEROES
                side = 1
            n_side[side] += 1
            x[hero_index] = 1
        if not (sum(x[:113]) == 5 and sum(x[113:]) == 5):
            print match['match_id']
            print n_side
            print x[:113]
            print x[113:]
            print sum(x[:113])
            print sum(x[113:])
        assert sum(x[:113]) == 5 and sum(x[113:]) == 5
    print 'Loading finished'
    # return X, y


def load_data(query=None):
    if not query:
        query = {}
    query_size = matches_collection.find(query).count()

    X = np.zeros((query_size, FEATURES)) 
    y = np.zeros(query_size)

    step = max(1, query_size / 20)
    print 'Loading data for query paramters {}, {} items'.format(query, query_size)
    for i, match in enumerate(matches_collection.find(query)):
        if i % step == 0:
            print '{:.0%}, {} matches loaded'.format(float(i)/query_size, i)
        if match['radiant_win']:
            y[i] = 1
        for player in match['players']:
            hero_index = player['hero_id'] - 1
            if player['player_slot'] > 127:
                # player is dire
                hero_index += HEROES
            X[i][hero_index] = 1
    print 'Loading finished'
    return X, y


def get_random_sample(X, y, sample_size=None):
    assert len(X) == len(y)
    if sample_size is None:
        sample_size = len(X)

    X_result = np.zeros((sample_size, FEATURES), dtype=np.int8)  
    y_result = np.zeros(sample_size, dtype=np.int8) 
    indexes = np.random.permutation(len(X))[:sample_size]
    for i, index in enumerate(indexes):
        X_result[i] = X[index]
        y_result[i] = y[index]
    return X_result, y_result


def make_train_test_data(X, y, file_name, sample_size=None):
    X_sample, y_sample = get_random_sample(X, y, sample_size)
    # 90% train, 10% test
    test_size = len(X_sample) / 10
    print 'Saving: {}, size {}'.format(file_name, len(X_sample))
    np.savez_compressed(
        '{}_train.npz'.format(file_name),
        X=X_sample[test_size:],
        y=y_sample[test_size:]
    )
    np.savez_compressed(
        '{}_test.npz'.format(file_name),
        X=X_sample[:test_size],
        y=y_sample[:test_size]
    )
    print 'Saved'


def save_all(X, y, file_name):
    print 'Saving: {}, size {}'.format(file_name, len(X))
    np.savez_compressed(
        file_name,
        X=X,
        y=y
    )
    print 'Saved'


def main():
    # UTC timestamp of January 20, 2016
    # take only matches before 20th January, as 6.86d balance patch came out
    # and it changes few things that can (maybe) bias data
    january_20 = 1453248000
    before_january = {'$lte': january_20}

    X_all, y_all = load_data({'start_time': before_january})
    make_train_test_data(X_all, y_all, 'all')

    # only ranked all pick
    X_all_ranked, y_all_ranked = load_data(
        {'game_mode': 22, 'start_time': before_january}
    )
    make_train_test_data(X_all_ranked, y_all_ranked, 'all_ranked')

    # everything besides ranked
    X_all_nonranked, y_all_nonranked = load_data(
        {'game_mode': {'$ne': 22}, 'start_time': before_january}
    )
    make_train_test_data(X_all_nonranked, y_all_nonranked, 'all_nonranked')


if __name__ == '__main__':
    setup()
    main()
    # check_data()