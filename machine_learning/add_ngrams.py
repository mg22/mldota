import sys
import pickle
import numpy as np
import util
import itertools
from time import time

from util import HEROES


def is_ngram_in(ngram, pick, offset=0):
    for hero in ngram:
        if not pick[offset + hero]:
            return False
    return True


def append_ngrams(X, ngrams):
    ngram_features = np.zeros((len(X), len(ngrams) * 2))
    for i, row in enumerate(X):
        for j, ngram in enumerate(ngrams):
            # d = d[1]
            # radiant
            if is_ngram_in(ngram, row):
                ngram_features[i][j] = 1.0
            # dire
            if is_ngram_in(ngram, row, HEROES):
                ngram_features[i][len(ngrams) +j] = 1.0
    return np.append(X, ngram_features, axis=1)    


def append_all_ngrams(X):
    for i in range(2, 4):
        ngrams = None
        with open('data/all_top250_atleast100_ngrams{}.pickle'.format(i), 'rb') as f:
            ngrams = pickle.load(f)
        X = append_ngrams(X, ngrams)
    return X


def main():
    X_train, y_train, X_test, y_test = util.load_data()

    print 'Appending digrams'
    start = time()
    X_train = append_all_ngrams(X_train)
    print list(X_train[0])
    X_test = append_all_ngrams(X_test)
    print 'Finished appending', time() - start

    util.save_data(X_train, y_train, X_test, y_test, 'all_top250_ngrams2_5')


if __name__ == '__main__':
    main()