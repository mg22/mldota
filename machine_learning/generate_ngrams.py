import pickle
from collections import defaultdict

import util
from util import HEROES


def process_pick(pick, result, ngram_map):
    heroes = util.extract_heroes(pick)
    assert len(heroes) == 5
    for s in util.powerset(sorted(heroes)):
        if len(s) == 0:
            continue
        ngram_map[s]['total'] += 1
        if result:
            ngram_map[s]['won'] += 1


def generate_ngram_winrate(X, y):
    ngram_map = defaultdict(lambda: {'total': 0, 'won': 0})

    # i = 0
    for x, result in zip(X, y):
        # if i % 1000 == 0:
            # print i
        # i += 1
        process_pick(x[:HEROES], result, ngram_map)
        process_pick(x[HEROES:], 1-result, ngram_map)

    return dict(ngram_map)


def generate_ngrams(ngram_map, fn, n_top=100, appearance=100):
    ngrams = [[] for _ in range(6)]
    for t in ngram_map.keys():
        stat = ngram_map[t]
        winrate = float(stat['won']) / stat['total']
        if stat['total'] > appearance:
            ngrams[len(t)].append((winrate, t))
    for i in range(2, 4):
        to_save = sorted(ngrams[i], reverse=True)[:n_top]
        # to_save[j] is (winrate, (id's of ngram heroes)
        to_save = [j[1] for j in to_save]
        print len(to_save), 'ngrams of size', i
        with open('data/all_top{}_atleast{}_ngrams{}.pickle'.format(n_top, appearance, i), 'wb') as f:
            pickle.dump(to_save, f)


def main():
    X_train, y_train, X_test, y_test = util.load_data()
    # generate map of ngram_tuple -> wins,appearences from X,y data
    ngram_map = generate_ngram_winrate(X_train, y_train)
    with open('data/all_train_winrate_map.pickle', 'wb') as f:
        pickle.dump(ngram_map, f)

    # generate file with n_top first 2/3-grams sorted by winrate 
    generate_ngrams(ngram_map, n_top=250)


if __name__ == '__main__':
    main()