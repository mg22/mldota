import sys
import pickle
import numpy as np
from time import time
from itertools import product
from sklearn import cross_validation
from sklearn.metrics import accuracy_score
from sklearn.svm import LinearSVC, SVC


import util

from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier

train_X, train_y, test_X, test_y = util.load_data()

# # loops manually rewritten few times
# size = 50000
# for i in [1, 2, 3, 4]:
#     for j in [200]:
#         # if (i == 1 and j == 50) or (i == 2 and j == 100):
#             # continue
#         print i, j
#         start = time()
#         ada = AdaBoostClassifier(DecisionTreeClassifier(max_depth=1, min_samples_leaf=i), n_estimators=j, learning_rate=1.0)
#         util.cross_val_metrics(ada, train_X[:size], train_y[:size])
#         print time() - start

ada = AdaBoostClassifier(DecisionTreeClassifier(max_depth=1), n_estimators=150, learning_rate=1.0)
util.cross_val_metrics(ada, train_X, train_y)

# ada.fit(train_X, train_y)
# predicted = ada.predict(test_X)
# util.print_metrics(test_y, predicted)