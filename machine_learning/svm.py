import sys
import pickle
import numpy as np
from time import time
from itertools import product
from sklearn import cross_validation
from sklearn.metrics import accuracy_score
from sklearn.svm import LinearSVC, SVC

# from hpsklearn import hyperopt_estimator
# import hpsklearn
# import hyperopt
from sklearn.preprocessing import Normalizer

import util


def hyperopt_svm(train_X, train_y, test_X, test_y):
    estimator = hpsklearn.HyperoptEstimator(
        # preprocessing=hpsklearn.components.any_preprocessing('pp'),
        classifier=hpsklearn.components.svc_rbf('aaa'),
        algo=hyperopt.tpe.suggest,
        trial_timeout=15.0, # seconds
        max_evals=15,
    )
    print 'Fitting'
    estimator.fit(train_X, train_y)
    print 'Fitting ended'
    model = estimator.best_model()
    print 'best model', model
    with open('rbf_svm_best_model.npy', 'wb') as f:
        pickle.dump(model, f)
    print 'Predicting'
    prediction = estimator.predict(test_X)
    print 'Predicition ended'
    score = estimator.score(prediction, test_y)
    print score


train_X, train_y, test_X, test_y = util.load_data()
size = 10000

svm = SVC(C=3.32918314263, cache_size=1000.0, class_weight=None, coef0=0.0,
  decision_function_shape=None, degree=3, gamma='auto', kernel='linear',
  max_iter=20206815, probability=False, random_state=3, shrinking=False,
  tol=0.142311440161, verbose=False)
svm.fit(train_X[:size], train_y[:size])
util.print_metrics(test_y, svm.predict(test_X))

# util.cross_val_metrics(svm, train_X[:size], train_y[:size])

# hyperopt_svm(train_X[:size], train_y[:size], test_X, test_y)

# svm = SVC(C=1132.12714012, cache_size=1000.0, class_weight=None, coef0=0.0,
#   decision_function_shape=None, degree=3, gamma=0.144618541955,
#   kernel='rbf', max_iter=487712367, probability=True, random_state=3,
#   shrinking=True, tol=1.48976198121e-05, verbose=False)
# preproc = Normalizer(copy=True, norm='l1')
# util.cross_val_metrics(svm, preproc.transform(train_X[:size]), train_y[:size])
