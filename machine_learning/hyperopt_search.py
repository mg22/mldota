import hpsklearn
import hyperopt
import pickle

from time import time
from sklearn.linear_model import SGDClassifier, LogisticRegression
from sklearn.preprocessing import StandardScaler
from sklearn.cross_validation import train_test_split
from util import load_data, load_all


def hyperopt_search(train_X, train_y, test_X, test_y):
    # let hyperopt choose classifier
    estimator = hpsklearn.HyperoptEstimator(
        # preprocessing=[],
        algo=hyperopt.tpe.suggest,
        trial_timeout=30.0, # seconds
        max_evals=100,
    )

    print 'Fitting'
    start = time()
    estimator.fit(train_X, train_y)
    print 'Fitting ended', time() - start

    model = estimator.best_model()
    print 'Best model', model
    with open('models/hyperopt_best.pickle', 'wb') as f:
        pickle.dump(model, f)

    score = estimator.score(test_X, test_y)
    print score


def main():
    train_X, train_y, test_X, test_y = load_data()
    size = 5000
    # X, y = load_all()
    # print len(X)
    # X = StandardScaler(copy=True, with_mean=True, with_std=True).fit_transform(X)
    # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1)
    # print X_train[:3]
    hyperopt_search(train_X[:size], train_y[:size], test_X, test_y)
    # model = SGDClassifier(alpha=0.00182113144862, average=False, class_weight=None,
    #    epsilon=0.1, eta0=0.0170945738612, fit_intercept=True,
    #    l1_ratio=0.120953106605, learning_rate='invscaling', loss='log',
    #    n_iter=5, n_jobs=1, penalty='l2', power_t=0.69159974748,
    #    random_state=None, shuffle=True, verbose=False, warm_start=False)
    # model = LogisticRegression()
    # model.fit(scaler.transform(train_X), train_y)
    # cross_val(model, X_train, y_train)
    # model.fit(X_train, y_train)
    # print model.score(X_test, y_test)

main()