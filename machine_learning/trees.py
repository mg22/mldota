import sys
import pickle
import numpy as np
from time import time
from itertools import product
from sklearn import cross_validation
from sklearn.metrics import accuracy_score
from sklearn.svm import LinearSVC, SVC
from sklearn.ensemble import RandomForestClassifier

import matplotlib.pyplot as plt

import hpsklearn
import hyperopt
from hyperopt import hp
from hyperopt.pyll import scope, as_apply

import util


def test_cross_val(estimator, X, y, scorer=None, cv=10):
    scores = cross_validation.cross_val_score(estimator, X, y, cv=cv, scoring=scorer)
    print 'accuracy:', scores.mean(), '+/-', scores.std() * 2 


def test(estimator, train_X, train_y, test_X, test_y):
    estimator.fit(train_X, train_y)
    predicted = estimator.predict(test_X)
    return accuracy_score(predicted, test_y)


def hyperopt_estimator(train_X, train_y, test_X, test_y):
    name = 'aaa'
    n_jobs = 1
    def _name(msg):
            return '%s.%s_%s' % (name, 'random_forest', msg)

    bootstrap_oob = hp.choice(_name('bootstrap_oob'),
                              [(True, True),
                               (True, False),
                               (False, False)])
    rval = scope.sklearn_RandomForestClassifier(
        n_estimators=scope.int(hp.quniform(
            _name('n_estimators'),
            1, 100, 1)),
        criterion=hp.choice(
            _name('criterion'),
            ['gini', 'entropy']),
        max_features=hp.choice(
            _name('max_features'),
            ['sqrt']),
        max_depth=None,
        min_samples_split=hp.quniform(
            _name('min_samples_split'),
            1, 10, 1),
        min_samples_leaf=hp.quniform(
            _name('min_samples_leaf'),
            1, 5, 1),
        bootstrap=bootstrap_oob[0],
        oob_score=bootstrap_oob[1],
        n_jobs=n_jobs,
        random_state=hp.randint(_name('rstate'), 5),
        verbose=False,
    )


    estimator = hpsklearn.HyperoptEstimator(
        # classifier=hpsklearn.components.random_forest('aaa', max_features=None),
        classifier=rval,
        algo=hyperopt.tpe.suggest,
        trial_timeout=15.0, # seconds
        max_evals=15,
    )
    print 'Fitting'
    estimator.fit(train_X, train_y)
    print 'Fitting ended'
    model = estimator.best_model()
    print 'best model', model
    with open('random_forest_best.npy', 'wb') as f:
        pickle.dump(model, f)
    # print 'Predicting'
    # prediction = estimator.predict(test_X)
    # print 'Predicition ended'
    score = estimator.score(test_X, test_y)
    print score


def feature_importance(forest, X, y):
    importances = forest.feature_importances_
    std = np.std([tree.feature_importances_ for tree in forest.estimators_],
                 axis=0)
    indices = np.argsort(importances)[::-1]

    # Print the feature ranking
    print("Feature ranking:")

    for f in range(X.shape[1]):
        print("%d. feature %d (%f)" % (f + 1, indices[f], importances[indices[f]]))

    # Plot the feature importances of the forest
    plt.figure()
    plt.title("Feature importances")
    plt.bar(range(X.shape[1]), importances[indices],
           color="r", yerr=std[indices], align="center")
    plt.xticks(range(X.shape[1]), indices)
    plt.xlim([-1, X.shape[1]])
    plt.show()

train_X, train_y, test_X, test_y = util.load_data()
size = len(train_X)


# clf = RandomForestClassifier(bootstrap=False, class_weight=None, criterion='gini',
#             max_depth=None, max_features=None, max_leaf_nodes=None,
#             min_samples_leaf=4.0, min_samples_split=4.0,
#             min_weight_fraction_leaf=0.0, n_estimators=34, n_jobs=3,
#             oob_score=False, random_state=2, verbose=False,
#             warm_start=False)
clf = RandomForestClassifier(bootstrap=True, class_weight=None, criterion='entropy',
            max_depth=None, max_features='sqrt', max_leaf_nodes=None,
            min_samples_leaf=3.0, min_samples_split=7.0,
            min_weight_fraction_leaf=0.0, n_estimators=32, n_jobs=1,
            oob_score=True, random_state=4, verbose=False,
            warm_start=False)

# future_importance(clf, train_X[:size], train_y[:size])
# util.cross_val_metrics(clf, train_X, train_y)

clf.fit(train_X[:size], train_y[:size])
predicted = clf.predict(test_X)
util.print_metrics(test_y, predicted)

# hyperopt_estimator(train_X[:size], train_y[:size], test_X, test_y)
