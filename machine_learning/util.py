import numpy as np
from sklearn import cross_validation
from sklearn.cross_validation import cross_val_predict
from sklearn import metrics
from itertools import chain, combinations


HEROES = 113


def load_data(dataset='all'):
    train = np.load('data/{}_train.npz'.format(dataset))
    test = np.load('data/{}_test.npz'.format(dataset))
    return train['X'], train['y'], test['X'], test['y']


def save_data(X_train, y_train, X_test, y_test, dataset='all'):
    np.savez_compressed(
        'data/{}_test.npz'.format(dataset),
        X=X_test,
        y=y_test
    )
    np.savez_compressed(
        'data/{}_train.npz'.format(dataset),
        X=X_train,
        y=y_train
    )


def load_all():
    data = np.load('data/all.npz')
    return data['X'], data['y']


def test_cross_val(estimator, X, y, scorer=None, cv=5):
    scores = cross_validation.cross_val_score(estimator, X, y, cv=cv, scoring=scorer)
    print 'accuracy:', scores.mean(), '+/-', scores.std() * 2 


def cross_val_metrics(clf, X, y, cv=5):
    predicted = cross_val_predict(clf, X, y, cv=cv)
    print 'accuracy:', metrics.accuracy_score(y, predicted)
    print 'precision:', metrics.precision_score(y, predicted)
    print 'recall:', metrics.recall_score(y, predicted)
    print 'roc_auc:', metrics.roc_auc_score(y, predicted)


def print_metrics(y_true, predicted):
    print 'accuracy:', metrics.accuracy_score(y_true, predicted)
    print 'precision:', metrics.precision_score(y_true, predicted)
    print 'recall:', metrics.recall_score(y_true, predicted)
    print 'roc_auc:', metrics.roc_auc_score(y_true, predicted)    


def powerset(iterable):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(len(s)+1))


def extract_heroes(pick):
    heroes = []
    for i, p in enumerate(pick):
        if p:
            heroes.append(i)
    return heroes


if __name__ == '__main__':
    pass