import pickle
import numpy as np
from time import time
import sys

from sklearn.tree import DecisionTreeRegressor
from sklearn.linear_model import Lasso
from sklearn.cross_validation import cross_val_score, train_test_split, KFold
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score, roc_auc_score
from sklearn import metrics
from sklearn.linear_model import Ridge
# import hpsklearn
# import hyperopt

import util
from util import HEROES
from generate_ngrams import generate_ngram_winrate
from generate_synergy import generate_synergy_score


def save_model():
    data = np.load('data/all_train_synergy.npz')
    X, y = data['X'], data['synergy']

    clf = Ridge()
    clf.fit(X, y)
    with open('models/synergy_ridge.pickle', 'wb') as f:
        pickle.dump(clf, f)


def tmp_f():
    data = np.load('data/all_train_synergy.npz')
    X, y = data['X'], data['synergy']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=0)

    size = len(X)

    # clf = AdaBoostRegressor(DecisionTreeClassifier(), n_estimators=100, learning_rate=1)
    # clf = RandomForestRegressor(n_estimators=50, min_samples_split=1) 
    # clf = Ridge()

    for i in [10, 50, 100, 200]:
        for j in [1, 2, 3]:
            # clf = AdaBoostRegressor(DecisionTreeClassifier(), n_estimators=i, learning_rate=j)
            # clf = RandomForestRegressor(n_estimators=i, min_samples_split=j) 
            clf = Ridge()
            scores = cross_val_score(clf, X_train, y_train)
            print 'cv score:', scores.mean()
            # clf.fit(X_train, y_train)
            # print 'test score:', clf.score(X_test, y_test)
            return


def predict(clf, X):
    radiant_synergy = clf.predict(X[:, :HEROES])
    dire_synergy = clf.predict(X[:, HEROES:])
    syn_predict = [(0.5 + (r-d)/2) for r, d in zip(radiant_synergy, dire_synergy)]
    predicted = [1 if p > 0.5 else 0 for p in syn_predict]
    return predicted


def synergy_crossval(weight):
    X_train, y_train, X_test, y_test = util.load_data()    
    
    acc, prec, rec, roc = [], [], [], []
    score = []
    k_fold = KFold(len(X_train), 5)
    for k, (train, test) in enumerate(k_fold):
        X_tr = X_train[train]
        y_tr = y_train[train]
        ngram_map = generate_ngram_winrate(X_tr, y_tr)
        X, s = generate_synergy_score(ngram_map, X_tr, weight)
        est = Ridge()
        est.fit(X, s)
        X_tt = X_train[test]
        y_tt = y_train[test]
        print len(X_tt), len(y_tt)
        predicted = predict(est, X_tt)

        acc.append(metrics.accuracy_score(y_tt, predicted))
        prec.append(metrics.precision_score(y_tt, predicted))
        rec.append(metrics.recall_score(y_tt, predicted))
        roc.append(metrics.roc_auc_score(y_tt, predicted))

        ngram_map = generate_ngram_winrate(X_tt, y_tt)
        X_t, s_t = generate_synergy_score(ngram_map, X_tt, weight)
        score.append(est.score(X_t, s_t))

    print 'accuracy:', np.mean(acc)
    print 'precision:', np.mean(prec)
    print 'recall:', np.mean(rec)
    print 'roc_auc:', np.mean(roc)
    print 'R2 score:', np.mean(score)


def synergy_crossval_best_weight():
    weights = [lambda x: 2**x, lambda x: 5**x, lambda x: 10**x] + [lambda x: x**2, lambda x: x**5, lambda x: x**10]
    for w in weights:
        print w
        synergy_crossval(w)


def synergy_test():
    X_train, y_train, X_test, y_test = util.load_data()
    clf = Ridge()
    ngram_map = generate_ngram_winrate(X_train, y_train)
    X, y = generate_synergy_score(ngram_map, X_train)
    clf.fit(X, y)

    p = predict(clf, X_test)
    util.print_metrics(y_test, p)



# synergy_train()
# synergy_crossval_best_weight()
synergy_test()
